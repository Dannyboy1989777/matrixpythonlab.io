---
title: "The Moderators"
description: "We are the moderation team of the Python Community on Matrix."
lead: "We are the moderation team of the Python Community on Matrix."
date: 2021-12-10T11:13:42+01:00
lastmod: 2021-12-10T11:13:42+01:00
contributors: ["Michael Sasser"]
draft: false
images: []
---

{{< moderators >}}

<p class="text-muted fst-italic fw-light" style="margin-top:-2.5em; margin-bottom:2em">
  If you click on a moderators name or avatar, you get their profile page
</p>
