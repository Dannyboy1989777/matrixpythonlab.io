---
title: "We are under Attack"
description: ""
lead: 'Due to a recent join spam attack, the community rooms are temporarily "invite only".'
date: 2022-03-01T08:18:00+01:00
lastmod: 2022-03-01T08:18:00+01:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: []
header_image: ""
floating_image: ""
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: ""
---

Everyone in a room can still invite other users. We apologize for the
inconvenience.
