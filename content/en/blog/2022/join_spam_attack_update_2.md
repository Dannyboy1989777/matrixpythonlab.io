---
title: "Join Spam Attack Update 2"
description: ""
lead: "First off, we would like to thank you all for your patience.
It was hard for us, too, not knowing if and how we can/should continue. 
Now we have some new information and solutions, we would like to share."
date: 2022-05-08T10:33:00+02:00
lastmod: 2022-05-08T10:33:00+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: []
header_image: ""
floating_image: ""
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: ""
---

### The Sync Problem

We now know why `matrix.org` is not syncing up. Thanks to Richard van der Hoff,
we now know, our room was blocked on `matrix.org` because of the "join spam"
attack, which took place a few days ago. There is a simple solution for that.
The matrix.org abuse team only needs to unblock the room, and we are back in
business.

As we have told in our last update, we heard rumors about other rooms being
affected by that attack as well. This rumor has been confirmed. So, we can
assume, this was not a targeted attack on our room.

Because of other communities being affected as well, we can also assume,
the abuse team is overwhelmed by the fallout. Therefore, we need to wait a
little bit longer until we receive some replies and get unblocked.

### How we mitigate an attack like this in the future

We would love to go into great detail about this topic, but an attacker
would be able to read this information as well. Therefore, we have decided
to keep this from you. We always had an open policy about what we are doing
and how we are doing things, but this time, we think, it is best for all, to
hold back this information.

The only piece of information we can share is, that we cannot prevent an
attack like this completely, but we can detect and mitigate it quite
effectively. For you, as community members, nothing changes.

### When will we go public again with the room?

As soon, as we get unblocked by the matrix.org abuse team, we will open the
gates again for new users.

## Update (3)

On May 10, 2022:

Apparently, we have been unblocked on `matrix.org`. If you still see a
different timeline, you can go to `All settings` -> `Help & About` and click
on `Clear cache and reload` (You will still be logged in afterwards). The
room is now public again. Thank you for your patience.

## Update (4)

On May 17, 2022:

This kick-wave should hopefully be the last one. We assume, we get it through
today. We think, we have no false-positives in there, but in case, you can
again afterwards (You are safe, when joined before Thu 28 April
2022 15:19:37 UTC). Since we don't rate limit the kicks anymore, this runs at
a much higher rate. So the room is a slower than the last time
