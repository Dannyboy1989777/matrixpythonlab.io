---
title: Contact
description: "Get in touch with the moderation team"
lead: "Get in touch with the moderation team and contributors"
date: 2021-12-10T11:13:42+01:00
lastmod: 2021-12-10T11:13:42+01:00
contributors: ["Michael Sasser"]
draft: false
images: []
---

<!-- TODO: Replace email address (2 times) -->

Depending on the nature of the contact request, we offer different contact
options.

Please keep in mind, the moderation team does their job on a hobby basis.
We try to be vigilant in the rooms and our projects, but we cannot guarantee
a 100% coverage at any time, especially with threads. If you want to report
any kind of violations or have questions, it might take some time until we
get back to you. In some cases, we like to discuss issues first, before
we respond.

<!-- Do not use Markdown in this accordion. Only HTML -->
{{< accordion html=true >}}

<!-- Code of Conduct Violations and Abuse Report -->
{{< accordion_item name="Code of Conduct Violations & Abuse Report" >}}
Unfortunately, if you currently report violations in any of our rooms, using
the report functionality, we don't get notified at all. We can't even see those
reports. They go directly to your homeserver's database and not meant to go
to the moderation team.
In the future, this should change with
<a
  target="_blank"
  rel="noopener"
  href="https://github.com/Yoric/matrix-doc/blob/aristotle/proposals/3215-towards-decentralized-moderation.md"
>
MSC3215
</a>,
but for now, we have to rely on other methods.<br />
<br />
Instead of pinging the moderators in a room, you can use our
<a href="{{< relref "helpdesk" >}}">helpdesk bot</a>
{{< identifier "@helpdesk:michaelsasser.org" >}}.<br />
<br />
<a href="{{< relref "helpdesk" >}}#using-the-helpdesk-bot">Using the Helpdesk Bot → </a><br />
<br />
If you are not already on
Matrix, you can send us an email
{{< identifier "abuse+pycommunity@michaelsasser.org" >}}.
{{< /accordion_item >}}

<!-- Organizational Questions -->
{{< accordion_item name="Organizational Questions" >}}
Since October 8, 2021 we have a room for exactly that. Please use our Meta room
{{< identifier "#python-meta:matrix.org" >}} on Matrix. If you are not already
on Matrix, you can send us an email
{{< identifier "info+pycommunity@michaelsasser.org" >}}.<br />
<br />
If you don't want to share your question with others, you can use our
<a href="{{< relref "helpdesk" >}}">helpdesk bot</a> for that.
<br />
<a href="{{< relref "helpdesk" >}}#using-the-helpdesk-bot">Using the Helpdesk Bot → </a>
{{< /accordion_item >}}

<!-- Programming Questions -->
{{< accordion_item name="Programming Questions" >}}
We do not answer Python questions directly.
Please use our Python room {{< identifier "#python:matrix.org" >}}.
{{< /accordion_item >}}

<!-- Programming Questions -->
{{< accordion_item name="Reporting Website Issues" >}}

Please check our docs for more information.

<a href="{{< ref "wiki/Help:Introduction.md#Reporting Issues" >}}">Reporting issues →</a>

{{< /accordion_item >}}

<!-- Questions About this Website -->
{{< accordion_item name="Questions About this Website" >}}

Please use our Meta room {{< identifier "#python-meta:matrix.org" >}}.
{{< /accordion_item >}}

<!-- Contact a Moderator Directly -->
{{< accordion_item name="Contact a Moderator Directly" >}}
If you like to talk to a specific moderator directly, you can find all
available contact informations on the
<a href="{{< relref "mods" >}}">Moderator Page</a> or on the moderators
profile, by clicking on the moderators name there.
{{< /accordion_item >}}

<!-- Contact a Contributor Directly -->
{{< accordion_item name="Contact a Contributor Directly" >}}
If you like to talk to a specific contributor directly, you can find all
available contact informations on the
<a href="{{< relref "contributors" >}}">Contributors Page</a> or on the contributor profile, by clicking on the contributors name there.
{{< /accordion_item >}}

<!-- Request to Unban a Homeserver -->
{{< accordion_item name="Request to Unban a Homeserver" >}}
If you are the owner of a banned homeserver, and you would like to have it
unbanned, contact our
<a href="{{< relref "helpdesk" >}}">helpdesk bot</a>
{{< identifier "@helpdesk:michaelsasser.org" >}}.<br />
<br />
<a href="{{< relref "helpdesk" >}}#using-the-helpdesk-bot">Using the Helpdesk Bot → </a><br />
<br />
{{< /accordion_item >}}

{{< /accordion >}}
