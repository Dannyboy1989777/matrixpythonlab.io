---
title: "Commands"
description: "The documentation about the moderation bot Mjölnir"
lead: "The documentation about the moderation bot Mjölnir"
date: 2021-10-8T20:17:00+00:00
lastmod: 2021-10-8T20:17:00+00:00
draft: false
images: ["element_reason.jpeg"]
menu:
  internal:
    parent: "mjolnir"
weight: 210
toc: true
---

{{< alert type="danger" >}}
Keep in mind, some commands listed below can do a massive
amount of damage with just a single command!<br /> Do not run any command, if
you don't know what it does. Make sure to read this documentation thoroughly
before using the bot.
{{< /alert >}}

To run a Mjölnir command, make sure you are in the Mjölnir command and control
room.

{{< alert type="success" title="Access to the Command and Control Room" >}}
If you cannot access the room, please contact
[Michael]({{< ref "michael-sasser" >}}).
{{< /alert >}}

Every command starts with a `prefix` Mjölnir accepts, see
[Prefixes →](#prefixes)<br /> Followed by the `command`, see
[Command Reference →]({{< ref "#command-reference" >}})<br /> The arguments after that depend
on the command which was used.

| Argument Placeholder     | Description                                                                 |
| ------------------------ | --------------------------------------------------------------------------- |
| `<list shortcode>`       | See [List Shortcode →]({{< ref "#list-shortcode" >}})                       |
| `<user>`                 | (Short for `<user ID>`)                                                     |
| `<room>`                 | The room identidier of the room e.g. `#room:domain.tld`                     |
| `<server>`               | The homeserver e.g. `domain.tld`                                            |
| `<glob>`                 | See [Globbing →](#globbing)                                                 |
| `[reason]`               | See [Reason →](#reason)                                                     |
| `<user ID>`              | The user identifier of the user e.g. `@user:domain.tld`                     |
| `[room alias/ID]`        | The room alias e.g. `RoomName` or identifier e.g. `#room:domain.tld`        |
| `[limit]`                | A limit as `integer` to limit the number of actions                         |
| `<event permalink>`      | A permalink to an event e.g. `$ECDPZSedVRUbAxsy0V-pHK5JIwGrO-XZsIo3m5zX-rI` |
| `<alias localpart>`      | For example `roomname` of `#roomname:domain.tld`                            |
| `<shortcode>`            | (Short for `<list shortcode>`)                                              |
| `<protection>`           | See [Protections →](#protections)                                           |
| `<target room alias/ID>` | The room identifier of the target room e.g. `#room:domain.tld`              |
| `[message]`              | A message as string                                                         |
| `<power level>`          | See [Power Level →](#power-level)                                           |

## Command Reference

These are the typical commands we use on a day-to-day basis. The prefix, that
is used in this documentation is `mjolnir`.

| Command                                                | Description                                                                                                                      |
| ------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `mjolnir status`                                       | Print status information                                                                                                         |
| `mjolnir ban <list shortcode> <user> <glob> [reason]`  | Adds an entity to the ban list                                                                                                   |
| `mjolnir unban <list shortcode> <user> <glob> [apply]` | Removes an entity from the ban list. If apply is 'true', the users matching the glob will actually be unbanned                   |
| `mjolnir redact <user ID> [room alias/ID] [limit]`     | Redacts messages by the sender in the target room (or all rooms), up to a maximum number of events in the backlog (default 1000) |
| `mjolnir redact <event permalink>`                     | Redacts a message by permalink                                                                                                   |
| `mjolnir kick <glob> [room alias/ID] [reason]`         | Kicks a user or all of those matching a glob in a particular room or all protected rooms                                         |
| `!mjolnir rules matching <user                         | room                                                                                                                             | server>` | Lists the rules in use that will match this entity e.g. `!rules matching @foo:example.com` will show all the user and server rules, including globs, that match this user |
| `mjolnir rules`                                        | Lists the rules currently in use by Mjolnir                                                                                      |
| `mjolnir help`                                         | This menu                                                                                                                        |

For more commands, use the command `mjolnir help` or see
[All Commands →](#all-commands)

## Prefixes

Every command starts with a prefix Mjölnir accepts.

- `!mjolnir` (default)
- `mjolnir`
- `bot`

{{< alert type="success" title="Adding and Removing Prefixes" >}}
Prefixes can only be added or removed during the deployment of Mjölnir. If
you want to change the list of reasons, please contact
[Michael]({{< ref "michael-sasser" >}}).
{{< /alert >}}

## List Shortcodes

A _list shortcode_ describes a short name for a room, which contains rules
about banned users, rooms or servers. Even though most rooms like this are
publicly readable only some users have the permissions to send some events to
those rooms.<br /> This means, by default, all users joining the room are
muted.

The list shortcodes we use, are in the following table. The permissions column
describes, the power level our Mjölnir instance has in that room.

| List Shortcode | Power Level | Description                                |
| -------------- | ----------- | ------------------------------------------ |
| `coc`          | `100`       | Shortcut for the Code of Conduct blacklist |
| `bs`           | `100`       | Shortcut for the Bad Server blacklist      |

Mjölnir can subscribe to lists or create new ones. When a user/room/server gets
added to a list, all subscribing instances of Mjölnir, immediately ban that
user, blacklist the room for local users (if Mjölnir is a homeserver
administrator and is configured that way) or sets room ACLs for the server.

Those actions are taken for every room a Mjolnir protects. When a user is not
in a room, Mjölnir will not ban the user right away. When a user joins a room
or, depending on the configuration, is invited to a room Mjölnir protects, it
checks, if a (ban) rule for that user exists. If the rule exists, Mjölnir takes
actions.

For more information about the power level, see [Power Level →](#power-level)

The `bs` blacklist only contains servers, which are non-malicious, but create
a high server load or generate countless error messages and are blacklisted from
federating with on the `michaelsasser.org` homeserver. Those severs are banned
from the community as well because Mjolnir, which also runs on
`michaelsasser.org`, can no longer see any events from them. Otherwise, this
would leave the room unprotected from those servers.
For more detailed information about this and the servers on that blacklist,
check out the
[Malicious & Misconfigured Servers](https://md.michaelsasser.org/s/hqyf-QAnV)
document.

### Room Reference

The table below describes which list shortcode describes which room.

| List Shortcode | Room                                                      |
| -------------- | --------------------------------------------------------- |
| `coc`          | {{< identifier "#python-coc:matrix.org" >}}               |
| `bs`           | {{< identifier "#python-bad-server:michaelsasser.org" >}} |

## Reason

A `reason` is a messages e.g. for a user, explaining the reason, why the user
that gets banned. This represents the same as the `reason` field used during
kicking or banning.

{{< img src="element_reason.jpeg" alt="element reason" caption="<center><em>Element Client: Reason for banning a user</em></center>" width="500" figure_class="border-0" >}}

For some _reasons_ Mjölnir can automatically redact (delete) the latest
messages of a user. Mjölnir currently redacts for the following reasons:

- `spam`
- `advertising`
- `advertisement`
- `ad`
- `scam`
- `abuse`
- `troll`
- `trolling`

Every other reason will not be redacted.

Reasons can only be added or removed during the deployment of Mjölnir. If you
want to change the list of reasons, please contact
[Michael]({{< ref "michael-sasser" >}}).

## Globbing

{{< alert type="danger" title="Globbing Warning" >}}
Globbing means dangerous business. Be aware of the effects this might have
beforehand!
<br />
Keep in mind, that this not only affect users, which are currently a part of
the community. It will affect users, who will join the community in the
future, too.
{{< /alert >}}

Globbing describes an ellipsis, or wildcard placeholder, which enables a
feature of Mjölnir to moderate every user who fits that pattern.

For example, you want to ban every user whose user ID starts with `spammer` and
is registered on the `matrix.org` homeserver:

```text
mjolnir ban coc @spam*:matrix.org spam --force`
```

Now, every user who fits that pattern will be banned. For example,
`@spam123:matrix.org`, `@spamabc:matrix.org` or `@spam:matrix.org`. The
`--force` argument is needed to tell Mjölnir that you really know what you are
about to do.

## Protections

Mjölnir offers automation to some extent, which means, it can perform certain
tasks on its own. Those tasks are called "Protections". The available
protections are listed below with their current status.

| Protection                      | Status   |
| ------------------------------- | -------- |
| `FirstMessageIsImageProtection` | disabled |
| `BasicFloodingProtection`       | enabled  |
| `WordList`                      | enabled  |
| `MessageIsVoiceProtection`      | enabled  |
| `MessageIsMediaProtection`      | disabled |
| `TrustedReporters`              | disabled |
| `DetectFederationLag`           | enabled  |
| `JoinWaveShortCircuit`          | enabled  |

None of the above listed protections are publishing the ban to any ban lists.

Even though, protections can be enabled/disabled from the _Command and Control
Room_, most settings cannot.

{{< accordion html=false >}}

<!-- FirstMessageIsImageProtection -->

{{< accordion_item name="FirstMessageIsImageProtection" >}}
If the first thing a user does after joining is to post an image or video,
they'll be banned for spam
{{< /accordion_item >}}

<!-- BasicFloodingProtection -->

{{< accordion_item name="BasicFloodingProtection" >}}
When a user posts ≥ 10 `m.room.message` events (messages, not lines) in 60
seconds, they'll be banned for spam with the same conditions as described
above.
{{< /accordion_item >}}

<!-- WordList -->

{{< accordion_item name="WordList" >}}
When an event occurs (e.g. someone writes a message), and the user who created
the event is in that room for less than 1440 minutes (1 day), the bot checks
`content.body` for keywords which are on the bad-word blacklist. If the bot
finds a match (regex), it will ban the user for spam, but only in the room the
user was creating that event. Likewise, it redacts ("deletes") the event
`m.room.message` with an event `m.room.redaction` without adding the user to
our "code of conduct" blacklist.

[Michael]({{< ref "/contributors/michael-sasser" >}}) then reviews the redacted
messages manually and unbans the user, in case it was a false-positive ban or
instruct the bot to add the user to "code of conduct" blacklist. When the time
is above 1440 minute mark (1 day), the bot "trusts" the user and ignores that
users `content.body`.

Currently, the following words are on the bad-word blacklist:

- mjolnirbanmenow
- GrapheneOS
- sugar daddy
- sugar baby
- people on how to earn
- https://i.imgflip.com/6pggbb.jpg
- hotmart.com
- thegreatwhitebrotherhood
- https://t.me/
- https://discord.gg/
- nigger
- hitler
- sexo
- faggot
- fag
- homo
- nigga
- penis
- blowjob
- vagina
- cunt
- cock
- coon
- anus

The first one "mjolnirbanmenow" is for testing purpose, so we don't have to
write any of the other words in any room, to test out, if the bot works
properly.

Changing settings or modifying words can only be done during the deployment
of Mjölnir. If you want to change anything, please contact
[Michael]({{< ref "michael-sasser" >}}).
{{< /accordion_item >}}

<!-- MessageIsVoiceProtection -->

{{< accordion_item name="MessageIsVoiceProtection" >}}
Because voice messages cannot be disabled, the bot redacts every voice message
without any side effect for the user (no kicking or banning)
{{< /accordion_item >}}

<!-- MessageIsMediaProtection -->

{{< accordion_item name="MessageIsMediaProtection" >}}

Because media in rooms cannot be disabled, the bot redacts every type of media
without any side effect for the user (no kicking or banning)
{{< /accordion_item >}}

<!-- TrustedReporters	 -->

{{< accordion_item name="TrustedReporters" >}}
{{< /accordion_item >}}

<!-- DetectFederationLag -->

{{< accordion_item name="DetectFederationLag" >}}
The bot count reports from trusted reporters and takes a configured action.
This feature is undocumented.
{{< /accordion_item >}}

<!-- JoinWaveShortCircuit -->

{{< accordion_item name="JoinWaveShortCircuit" >}}
This protection protects the room from join spam.
When $ u $ users join in $ t $ minutes, the room will be set to invite-only.

### Configuration Options

| Configuration                           | Default | Currently Set | Unit    | Symbol |
| --------------------------------------- | ------- | ------------- | ------- | ------ |
| `JoinWaveShortCircuit.maxPer`           | 50      | 30            | users   | $ u $  |
| `JoinWaveShortCircuit.timescaleMinutes` | 60      | 12            | minutes | $ t $  |

[Learn more about how to use protection settings →](#configuration)
{{< /accordion_item >}}

{{< /accordion >}}

## Configuration

{{< alert type="warning" title="Configuration" >}}
`!mjolnir config get <Protection>` doesn't seem to work.
{{< /alert >}}

Some protections can be configured from within the command and control
room. Those options are set or queried by a key, as they are stored in a
state event, key-value pair.

The syntax looks like the following:

```text
!mjolnir config get <Protection>
!mjolnir config set <Protection>.<Key> <Value>
```

The first line gets the all configuration options of the protection
`<Protection>`, the second line sets the value `<Value>` of key `<Key>`.

Some Protections hold an array (or list) of values instead of a single value.
From that array of values, a value can be added or removed.

```text
!mjolnir config add <Protection>.<Key> <Value>
!mjolnir config remove <Protection>.<Key> <Value>
```

The first line adds the value `<Value>` to the array of values of key
`<Key>`, the second line removes the value `<Value>` from the array of values
of the key `<Key>`.

[Check out an example how to get and set a value →]({{< relref "examples" >}}#example-set-maxper-of-basicfloodingprotection)

## Power Level

Power levels are used in the room permission system. Every new user starts with
a power level of `0` (default setting).

The table below shows the named power levels of a room, which uses the default
settings.

| Power Level | Name          | Description                                                    |
| ----------- | ------------- | -------------------------------------------------------------- |
| `100`       | Administrator |                                                                |
| `50`        | Moderator     |                                                                |
| `0`         | Default       | what every new user gets                                       |
| `-1`        | Muted         | The user cannot write messages, but can still send `reactions` |

## All Commands

{{< alert >}}
Some of the following commands cannot be used with this instance
of Mjölnir because it is no longer a homeserver administrator.
{{< /alert >}}

| Command                                                              | Description                                                                                                                      |
| -------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `mjolnir`                                                            | Print status information                                                                                                         |
| `mjolnir status`                                                     | Print status information                                                                                                         |
| `mjolnir ban <list shortcode> <user\|room\|server> <glob> [reason]`  | Adds an entity to the ban list                                                                                                   |
| `mjolnir unban <list shortcode> <user\|room\|server> <glob> [apply]` | Removes an entity from the ban list. If apply is 'true', the users matching the glob will actually be unbanned                   |
| `mjolnir redact <user ID> [room alias/ID] [limit]`                   | Redacts messages by the sender in the target room (or all rooms), up to a maximum number of events in the backlog (default 1000) |
| `mjolnir redact <event permalink>`                                   | Redacts a message by permalink                                                                                                   |
| `mjolnir kick <glob> [room alias/ID] [reason]`                       | Kicks a user or all of those matching a glob in a particular room or all protected rooms                                         |
| `!mjolnir rules matching <user                                       | room                                                                                                                             | server>` | Lists the rules in use that will match this entity e.g. `!rules matching @foo:example.com` will show all the user and server rules, including globs, that match this user |
| `mjolnir rules`                                                      | Lists the rules currently in use by Mjolnir                                                                                      |
| `mjolnir sync`                                                       | Force updates of all lists and re-apply rules                                                                                    |
| `mjolnir verify`                                                     | Ensures Mjolnir can moderate all your rooms                                                                                      |
| `mjolnir list create <shortcode> <alias localpart>`                  | Creates a new ban list with the given shortcode and alias                                                                        |
| `mjolnir watch <room alias/ID>`                                      | Watches a ban list                                                                                                               |
| `mjolnir unwatch <room alias/ID>`                                    | Unwatches a ban list                                                                                                             |
| `mjolnir import <room alias/ID> <list shortcode>`                    | Imports bans and ACLs into the given list                                                                                        |
| `mjolnir default <shortcode>`                                        | Sets the default list for commands                                                                                               |
| `mjolnir deactivate <user ID>`                                       | Deactivates a user ID                                                                                                            |
| `mjolnir protections`                                                | List all available protections                                                                                                   |
| `mjolnir enable <protection>`                                        | Enables a particular protection                                                                                                  |
| `mjolnir disable <protection>`                                       | Disables a particular protection                                                                                                 |
| `mjolnir config set <protection>.<setting> [value]`                  | Change a projection setting                                                                                                      |
| `mjolnir config add <protection>.<setting> [value]`                  | Add a value to a list protection setting                                                                                         |
| `mjolnir config remove <protection>.<setting> [value]`               | Remove a value from a list protection setting                                                                                    |
| `mjolnir config get [protection]`                                    | List protection settings                                                                                                         |
| `mjolnir rooms`                                                      | Lists all the protected rooms                                                                                                    |
| `mjolnir rooms add <room alias/ID>`                                  | Adds a protected room (may cause high server load)                                                                               |
| `mjolnir rooms remove <room alias/ID>`                               | Removes a protected room                                                                                                         |
| `mjolnir move <room alias> <room alias/ID>`                          | Moves a <room alias> to a new <room ID>                                                                                          |
| `mjolnir directory add <room alias/ID>`                              | Publishes a room in the server's room directory                                                                                  |
| `mjolnir directory remove <room alias/ID>`                           | Removes a room from the server's room directory                                                                                  |
| `mjolnir alias add <room alias> <target room alias/ID>`              | Adds <room alias> to <target room>                                                                                               |
| `mjolnir alias remove <room alias>`                                  | Deletes the room alias from whatever room it is attached to                                                                      |
| `mjolnir resolve <room alias>`                                       | Resolves a room alias to a room ID                                                                                               |
| `mjolnir shutdown room <room alias/ID> [message]`                    | Uses the bot's account to shut down a room, preventing access to the room on this server                                         |
| `mjolnir powerlevel <user ID> <power level> [room alias/ID]`         | Sets the power level of the user in the specified room (or all protected rooms)                                                  |
| `mjolnir make admin <room alias> [user alias/ID]`                    | Make the specified user or the bot itself admin of the room                                                                      |
| `mjolnir help`                                                       | This menu                                                                                                                        |
