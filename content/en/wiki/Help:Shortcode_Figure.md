---
title: "Help:Shortcode Figure"
url: "/wiki/Help:Shortcode_Figure"
description: ""
lead: "Display something inside an HTML figure."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Figure
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`figure`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display something inside an HTML figure.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

`figure` is a nested shortcode which produces a HTML figure.

## Parameters

The figure shortcode has the following parameters:

| Parameter      | Description                          |
| -------------- | ------------------------------------ |
| `figure_class` | Add classes to the figure            |
| `img_class`    | Add classes to the image             |
| `width`        | Change the default width             |
| `caption`      | The caption                          |
| `.Inner`       | Whatever will be shown in the figure |

## Examples

```md
{{</* figure caption="The caption line" */>}}
  Hello World
{{</* /figure */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  {{< figure caption="The caption line" >}}
    Hello World
  {{< /figure >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/figure.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/figure.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Figure Shortcode" path="/layouts/shortcodes/figure.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_img.scss`

The `figure` shortcode does not have it's own style. It uses the style of
the `img` shortcode, as it is derived from it.

{{< ln "wiki/Help:Shortcode_Img" >}}

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_img.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Image Style" path="/assets/scss/components/_shortcode_img.scss" >}}
