---
title: "Help:Blog"
url: "/wiki/Help:Blog"
description: ""
lead: "Create a Blog Post."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

## Location

| Directory                                              | Description                    |
| ------------------------------------------------------ | ------------------------------ |
| [./content/\<Language\>/blog/]({{< reporef "blog" >}}) | The base directory of the blog |

{{% newpage name="blog" by_year=true %}}

## Front Matter

{{< variable_structure "Blog" "title" "description" "lead" "date" "lastmod" "draft" "weight" "images" "contributors" "floating_image" "floating_image_width" "floating_image_position" "floating_image_caption" "header_image" >}}

Notice, the `weight` in blog posts is always `50`.

## Code

Below you find the implementation of the layout.

### HTML

#### Single Page Layout

Defined in `layouts/blog/single.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/blog/single.html" %}}

#### List Page Layout

Defined in `layouts/blog/list.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/blog/list.html" %}}

### SCSS

Defined in `assets/scss/layouts/_blog.scss`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="assets/scss/layouts/_blog.scss" %}}

### Archetype

Defined in `archetypes/blog.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/blog.md" type="yaml" %}}
