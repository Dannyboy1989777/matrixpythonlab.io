---
title: "Help:Shortcode Rendered"
url: "/wiki/Help:Shortcode_Rendered"
description: ""
lead: "Display something in a card to show an example."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Rendered
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`rendered`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display "whatever" is in `.Inner` inside a demo-box.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

`rendered` produces a box around a rendered object with the word _Rendered_ on
top. It is used only on this site to show rendered shortcodes in a box.

## Parameters

The rendered shortcode has the following parameters:

| Parameter | Description           |
| --------- | --------------------- |
| `.Inner`  | The object to display |

## Examples

```md
{{</* rendered */>}}
  Hello
{{</* /rendered */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  Hello
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/rendered.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/rendered.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Rendered Shortcode" path="/layouts/shortcodes/rendered.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_rendered.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_rendered.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Rendered Style" path="/assets/scss/components/_shortcode_rendered.scss" >}}
