---
title: "Help:Introduction"
description: "Learn to create and edit pages"
lead: "Learn to create and edit pages"
url: "/wiki/Help:Introduction"
date: 2022-06-06T09:40:00+20:00
lastmod: 2022-06-06T09:40:00+20:00
contributors: ["Michael Sasser"]
wiki_categories: ["help"]
draft: false
images: []
weight: 100
toc: true
menu:
  wiki:
    name: Learn to edit
    parent: "wiki_contribute"
    weight: 20
  wiki_categories:
    name: Learn to edit
    parent: "wiki_categories_contribute"
    weight: 20
---

{{< alert preset="coc" >}}{{< /alert >}}

Our website, as well as out wiki are collaboratively edited by the community.
They are meant as a platform to collect information about our community,
Matrix, the Python programming language in general and useful information.

## Report Issues

If you found an problem or you are missing a feature, please file a
[Bug Report or Feature Request
→](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/issues/new)

## Edit Workflow

Because our website is a static website, edits are done using familiar tools,
such as `git`, `git-lfs` and your favorite editor. The pages are written
in [Markdown](https://en.wikipedia.org/wiki/Markdown), a lightweight markup
language for creating formatted text.
To make a change or create a new wiki page, you need to create an issue first
and a merge request (pull request) afterwards to our
[GitLab repository](https://gitlab.com/matrixpython/matrixpython.gitlab.io/).

Please check out the workflow, on how to work with the page, before making
any modifications.

{{< ln "wiki/Help:Workflow.md" >}}
{{< ln "wiki/Help:Triage_Process.md" >}}

## Image Conventions

Our convention is to use a `svg` image whenever possible. If this is not
possible, you use a `png` image, if your image contains an alpha channel. If
not use an `jpeg` image or an `gif`, if your image only references a low number
of colors. Other formats are only accepted if they are used for a special
purpose.

## Section Documentation

The [content/]({{< reporef "content" >}}) directory is the base directory for
all content.

| Section                                                          | Documents                                  |
| ---------------------------------------------------------------- | ------------------------------------------ |
| [Docs]({{< relref "wiki/Help:Docs.md" >}})                       | The documentation                          |
| [Blog]({{< relref "wiki/Help:Blog.md" >}})                       | Blog posts                                 |
| [Categories]({{< relref "wiki/Help:Categories.md" >}})           | **Blog** categories                        |
| [Tags]({{< relref "wiki/Help:Tags.md" >}})                       | **Blog** Tags                              |
| [Changelog]({{< relref "wiki/Help:Changelog.md" >}})             | The "Community Changelog" page and entries |
| [Contributors]({{< relref "wiki/Help:Contributors.md" >}})       | Contributor personal profile pages         |
| [Privacy-Policy]({{< relref "wiki/Help:Privacy-Policy.md" >}})   | The privacy-policy (imprint and GDPR)      |
| [Wiki]({{< relref "wiki/Help:Wiki.md" >}})                       | The Wiki                                   |
| [Wiki Categories]({{< relref "wiki/Help:Wiki_Categories.md" >}}) | Wiki Categories                            |
| [Contact]({{< relref "wiki/Help:Contact.md" >}})                 | The Contact Page                           |
| Moderators                                                       | The Moderators Page                        |
| Sponsors                                                         | The Sponsors page                          |

The landing page itself is written in _HTML_ and is located in
the `layout/` directory as `index.html`. The same location is true for the
`robots.txt`, `rss.xml`, `sidemap.xml` and the error pages.

## Shortcodes

Shortcodes are small snippets of code that are used to insert content into a
page. The following shortcodes are written by us to be used to extend the
capabilities of the pages, which are created as Markdown files.

| Shortcodes                                                                       | Description                                                             |
| -------------------------------------------------------------------------------- | ----------------------------------------------------------------------- |
| [Accordion]({{< relref "wiki/Help:Shortcode_Accordion.md" >}})                   | Insert your content into an accordion                                   |
| [Alert]({{< relref "wiki/Help:Shortcode_Alert.md" >}})                           | Create user defined alerts in a pager or use a preset                   |
| [Badge]({{< relref "wiki/Help:Shortcode_Badge.md" >}})                           | Create inline badges                                                    |
| [Contributors]({{< relref "wiki/Help:Shortcode_Contributors.md" >}})             | Display the list of contributors                                        |
| [Details]({{< relref "wiki/Help:Shortcode_Details.md" >}})                       | Hide content in an collapsed block                                      |
| [Dot]({{< relref "wiki/Help:Shortcode_Dot.md" >}})                               | Display a red dot with a number in it                                   |
| [Download]({{< relref "wiki/Help:Shortcode_Download.md" >}})                     | Allow users to download a file                                          |
| [Figure]({{< relref "wiki/Help:Shortcode_Figure.md" >}})                         | Place a HTML figure around your content                                 |
| [GitLab Label]({{< relref "wiki/Help:Shortcode_GitLab_label.md" >}})             | Cerate a badge, which looks like a GitLab label                         |
| [Identifier]({{< relref "wiki/Help:Shortcode_Identifier.md" >}})                 | Create an identifier pill with matrix rooms, users and email addresses  |
| [Img]({{< relref "wiki/Help:Shortcode_Img.md" >}})                               | Place a zoomable image with an optional figure                          |
| [Ln]({{< relref "wiki/Help:Shortcode_Ln.md" >}})                                 | Create a link box, which shows a truncated lead of the page in a path   |
| [Mermaid]({{< relref "wiki/Help:Shortcode_Mermaid.md" >}})                       | Draw and place a mermaid graph                                          |
| [Moderators]({{< relref "wiki/Help:Shortcode_Moderators.md" >}})                 | Display the list of moderators                                          |
| [Post]({{< relref "wiki/Help:Shortcode_Post.md" >}})                             | Display an, optionally linked, post from the blog                       |
| [Readfile]({{< relref "wiki/Help:Shortcode_Readfile.md" >}})                     | Crate a code block with source code from a file                         |
| [Ref]({{< relref "wiki/Help:Shortcode_Ref.md" >}})                               | Create absolute references e.g. for links                               |
| [Relref]({{< relref "wiki/Help:Shortcode_Relref.md" >}})                         | Create relative references e.g. for links                               |
| [Rendered]({{< relref "wiki/Help:Shortcode_Rendered.md" >}})                     | Place a "Rendered" box around your content                              |
| [Reporef]({{< relref "wiki/Help:Shortcode_Reporef.md" >}})                       | Create a string, which can be used as link to the repo for a local path |
| [Reporting Issues]({{< relref "wiki/Help:Shortcode_Reporting_Issues.md" >}})     | Show users how they can report Code of Conduct violations               |
| [Tabs]({{< relref "wiki/Help:Shortcode_Tabs.md" >}})                             | Place your content in tabs                                              |
| [Variable Structure]({{< relref "wiki/Help:Shortcode_Variable_Structure.md" >}}) | Create an example front matter (for documentation purpose)              |

## Partials

What Shortcodes are for Markdown, Partials are for HTML. They are small
snippets of code that are used to insert content into a page.

### Footer

| Partials      | Description                           |
| ------------- | ------------------------------------- |
| footer        | The footer of the page                |
| script-footer | The scripts in the footer of the page |

### Head

| Partials        | Description                                                           |
| --------------- | --------------------------------------------------------------------- |
| favicons        | The pages favicons                                                    |
| head            | The head of the page                                                  |
| opengraph       | The opengraph meta tags                                               |
| resource-hints  | Resource hints such as font preloads                                  |
| script-header   | The scripts in the header of the page                                 |
| seo             | The SEO meta tags                                                     |
| structured-data | Structured data for search engines understand the content of the page |
| stylesheet      | The stylesheet of the page                                            |
| twitter_cards   | Twitter Cards, you can attach rich media experiences to Tweets        |

### Header

| Partials | Description                              |
| -------- | ---------------------------------------- |
| alert    | Alerts to show above the navbar          |
| header   | The header (e.g. the navbar) of the page |

### Main

| Partials                  | Description                                                             |
| ------------------------- | ----------------------------------------------------------------------- |
| blog-meta                 | A line of information of the blog, such as the authors and publish date |
| blog-navigation           | The blog's pagination elements                                          |
| blog-post-list-layout     |                                                                         |
| blog-sidebar              |                                                                         |
| blog-widget-archives      |                                                                         |
| blog-widget-categories    |                                                                         |
| blog-widget-tags          |                                                                         |
| breadcrumb                |                                                                         |
| date                      |                                                                         |
| docs-navigation           |                                                                         |
| edit-page                 |                                                                         |
| embedded_image            |                                                                         |
| fast-latex-icon           |                                                                         |
| last-modified             |                                                                         |
| wiki-category-list-layout |                                                                         |
| wiki-infobox              |                                                                         |

### Sidebar

| Partials                | Description |
| ----------------------- | ----------- |
| auto-collapsible-menu   |             |
| auto-default-menu       |             |
| docs-menu               |             |
| docs-toc                |             |
| internal-menu           |             |
| manual-collapsible-menu |             |
| manual-default-menu     |             |
| show-rss                |             |
| wiki-toc                |             |
