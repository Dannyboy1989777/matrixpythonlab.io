---
title: "Help:Tags"
url: "/wiki/Help:Tags"
description: ""
lead: "Create a blog tags."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---


Blog tags are a way to group blog posts in a way, users are able to
filter for. Our tags are not like our categories, which can have their own
image, description and page. They are created on the fly, when used. They
should only consist of a single word.

## Code

Below you find the implementation of the layout.

### HTML

#### Term Page Layout

Defined in `layouts/tags/term.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/tags/term.html" %}}

#### Terms Page Layout

Defined in `layouts/tags/terms.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/tags/terms.html" %}}
