---
title: "Help:Wiki_Categories"
url: "/wiki/Help:Wiki_Categories"
description: ""
lead: "Create a blog tags."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

Wiki categories are a way to group wiki pages in a way, users are able to
filter for. Our wiki categories currently behave like blog tags.
They can produce a list of wiki pages in a category, but are not yet editable.
In the future, we plan to extend them to be more like a wiki page, which
includes a list of wiki categories. Currently, categories are created on the
fly, when they are used. This behavior will not change in the future.
Users will only be able to add additional context to them.

## Code

Below you find the implementation of the layout.

### HTML

#### Term Page Layout

Defined in `layouts/wiki_categories/term.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/wiki_categories/term.html" %}}

#### Terms Page Layout

Defined in `layouts/wiki_categories/terms.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/wiki_categories/terms.html" %}}
