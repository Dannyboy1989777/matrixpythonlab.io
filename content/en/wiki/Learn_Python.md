---
title: "Learn Python"
url: "/wiki/Learn_Python/"
description: "Find out, how to learn Python"
lead: "Find out, how to learn Python"
date: 2022-11-17T12:10:41+01:00
lastmod: 2022-11-17T12:10:41+01:00
contributors: []
draft: false
images: []
weight: 50
toc: true
---

One of the most frequently asked question in our Community, is how to learn
Python? To answer this question, we created list of materials for beginners.

You can find a more complete list of possible learning materials on the
[Python wiki](https://wiki.python.org/moin/BeginnersGuide/NonProgrammers).

## Books

### Automate the Boring Stuff with Python

{{< badge title="Free Online" type="danger" shape="square" >}}
{{< badge title="Paid Book" type="primary" shape="square" >}}

<div class="text-center pb-4">
  <!-- TODO: Use the cover as image; Ask for using it -->
  <!-- <img src="https://automatetheboringstuff.com/images/cover_automate2_thumb.jpg"></img> -->
</div>

"Automate the Boring Stuff with Python" by Al Sweigart et al. is the most
recommended books in the Community. It focuses on people who have never written
any line of code and teaches them the basics of Python.

<br /><br />

Website: [https://automatetheboringstuff.com](https://automatetheboringstuff.com/)

ISBN-13: 978-1-59327-992-9

## Videos

### YouTube: Corey Schafer's Python Tutorials

{{< badge title="Free Course" type="danger" shape="square" >}}

<br /><br />
Corey Schafer created well over 100, free to watch, videos about Python,
dedicated to newcomers.
In addition they feature many free courses for `Pandas`, `Matplotlib`, `Flask`,
`Django`, even `git` and `SQL`, and many more.

<br /><br />

YouTube Channel: [https://www.youtube.com/c/Coreyms/featured](https://www.youtube.com/c/Coreyms/featured)
