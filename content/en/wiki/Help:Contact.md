---
title: "Help:Contact"
url: "/wiki/Help:Contact"
description: ""
lead: "Show our contact details."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

## Location

| Directory                                                  | Description      |
| ---------------------------------------------------------- | ---------------- |
| [content/\<Language\>/contact/]({{< reporef "contact" >}}) | Our contact page |

The contact page shows a user how to contact us.

## Code

The contact page does not have it's own layout. It uses our default single
page layout.
