---
title: "Help:Shortcode Moderators"
url: "/wiki/Help:Shortcode_Moderators"
description: ""
lead: "Display a table of moderators with their avatar in front of their names."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Moderators
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`moderators`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a list of moderators with an avatar in front of their names.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

The moderators shortcode renders a table of all moderators from the data
entered in the front matter of their contributor page.

For more information on how to create a contributor, check out the
Contributor documentation.

{{< ln "wiki/Help:Contributors" >}}

## Parameters

The `moderators` shortcode has no parameters.

## Examples

```md
{{</* moderators */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
  {{< moderators >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/moderators.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/moderators.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Moderators Shortcode" path="/layouts/shortcodes/moderators.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_contributors.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

The `moderators` shortcode uses the styles from the `contributors` shortcode,
as it is derived from it.

{{< ln "wiki/Help:Shortcode_Contributors" >}}

{{% readfile path="/assets/scss/components/_shortcode_contributors.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Contributors Style" path="/assets/scss/components/_shortcode_contributors.scss" >}}
