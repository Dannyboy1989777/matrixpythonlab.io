---
title: "Help:Shortcode Ln"
url: "/wiki/Help:Shortcode_Ln"
description: ""
lead: "Create a link which shows the page name and lead, it links to."
date: 2022-08-11T09:43:00+02:00
lastmod: 2022-08-11T09:43:00+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Ln
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`ln`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a box like button which shows the page name and lead, it links
        to
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

Use the shortcode `ln` to display a box which works like a button and shows
the page name in the front and the page lead in small at the end.

## Parameters

The Ln shortcode has the following parameters:

| Parameter | Description                                                                                                                                                                                       |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `[0]`     | The path to a page, with or without a file extension, with or without an anchor. A path without a leading `/` is first resolved relative to the given context, then to the remainder of the site. |

## Examples

```md
{{</* ln "wiki/Help:Shortcode_Ln" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< ln "wiki/Help:Shortcode_Ln" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/ln.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/ln.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Ln Shortcode" path="/layouts/shortcodes/ln.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_ln.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_ln.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Ln Style" path="/assets/scss/components/_shortcode_ln.scss" >}}
