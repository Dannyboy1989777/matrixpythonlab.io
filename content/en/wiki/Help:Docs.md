---
title: "Help:Docs"
url: "/wiki/Help:Docs"
description: ""
lead: "Create a wiki page."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

## Location

| Directory                                                                           | Description                               |
| ----------------------------------------------------------------------------------- | ----------------------------------------- |
| [content/\<Language\>/docs/]({{< reporef "docs" >}})                                | The base directory for the docs           |
| [content/\<Language\>/docs/prologue]({{< reporef "docs/prologue" >}})               | The "Prologue"section of the docs         |
| [content/\<Language\>/docs/tutorial]({{< reporef "docs/tutorial" >}})               | The "Tutorial" section of the docs        |
| [content/\<Language\>/docs/code_of_conduct]({{< reporef "docs/code_of_conduct" >}}) | The "Code of Conduct" section of the docs |
| [content/\<Language\>/docs/help]({{< reporef "docs/help" >}})                       | The "Help" section of the docs            |

For an documentation page the menu structure is:

```yaml
menu:
  docs:
    parent: "<the parent>"  # e.g. "prologue", "tutorial", "help", ...
```

By convention, `<the parent>` is the directory name of the last directory in
the directory path the file is created in, if the path was not inside a
[page-bundle](https://gohugo.io/content-management/page-bundles/). This is
because of our directory structuring convention. If this doesn't work, the
directory structure is wrong and must be corrected. Either in the directory
structure or in the `config/_default/menus.toml`.

To follow our convention we go on with weights. The `weight` sets the order,
the docs are shown to the user. You find our order (`weight`) convention in
`config/_default/menus.toml` For example the Tutorial has the `weight = 20` in
the `config/_default/menus.toml`, so the weight of the tutorial starts with `2`
(ignore the `0` in `20`, we are only interested in the `2`). Then the first
entry will get the `weight = 200`. The one after that will get `weight = 210`,
then `weight = 220` and so on.

{{% newpage name="docs" by_section=true %}}

## Front Matter

{{< variable_structure "Documentation" "title" "description" "lead" "date" "lastmod" "contributors" "draft" "images" "menu" "weight" "toc" >}}

## Code

Below you find the implementation of the layout.

### HTML

#### Single Page Layout

Defined in `layouts/docs/single.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/docs/single.html" %}}

#### List Page Layout

Defined in `layouts/docs/list.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/docs/list.html" %}}

### Archetype

Defined in `layouts/docs.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/docs.md" type="yaml" %}}
