---
title: "Banned Servers"
url: "/wiki/Banned_Servers/"
description: "A list of homeservers, which are banned from the community"
lead: "Due to code of conduct violations the following homeservers are banned from our community."
date: 2022-06-22T21:06:45+02:00
lastmod: 2022-06-22T21:06:45+02:00
contributors: ["Michael Sasser"]
categories: ["Community"]
draft: false
images: []
weight: 50
toc: true
---

If you are a user on one of those servers, you will not be able to join our
community. If you are the owner of the homeserver, and you would like to
request your server to be unbanned, please [contact]({{< ref "contact" >}}) us.

<!-- TODO: Automate the creation of that list -->

Server list:

- `foxears.life`
- `fjibby.org`
- `fmxroute.com`
- `firontruth.xyz`
- `fdissected.us`
- `fspectreos.de`
- `fm4geek.com`
- `fring-0.net`
- `fx1101.net`
- `fselfhost.co`
- `fgeonat.nz`
- `fverstehbahnhof.de`
- `fvanderpot.net`
- `fzitech.cyou`
- `fgeonat.nz`
- `fatlmesh.net`
- `fvergeylen.eu`
- `flolispace.moe`
- `fwaifuhunter.club`
- `fmasterislonely.xyz`
- `frenaissancemen.xyz`
- `fcitadel7.org`
- `fdissected.us`
- `fzom.im`
- `fziguana.club`
- `fip-t.com`
- `ftrustbtc.org`
- `fchacah.io`
- `flyberry.com`
- `fverstehbahnhof.de`
- `florenzo12floxy.ga`
- `fkiwifarms.net`
- `fm4geek.com`
- `fgorghul.de`
- `fnett.media`
- `frankenste.in`
- `fportl.live`
- `fampextech.ddns.net`
- `fkoenhendriks.nl`
- `fring-0.net`
- `fspectreos.de`
- `fmerrymount.family`
- `fip-t.com`
- `fkeanu.im`
- `fgyro-tech.net`
- `fnigga.no`
- `fsympa.city`
- `fcactus.chat`
- `fpclovers.su`
- `fgeonat.nz`
- `fx1101.net`
- `frong.zone`
- `fdp15.us`
- `fportl.live`
- `flongtimeno.cyou`
- `firontruth.tk`
- `flolicon.rocks`
- `fchickenkiller.com`
- `flat3st.de`
- `fadfontesapertas.de`
- `fgyro-tech.net`
- `fzom.im`
- `fnett.media`
- `fjlscode.com`
- `fcalamari.space`
- `fdp15.us`
- `fchickenkiller.com`
- `fmoc.network`
- `fselfhost.co`
- `faristillus.xyz`
- `fmms.lol`
- `florenzo12floxy.ga`
- `ffoxears.life`
- `fclimatestrike.ch`
- `fjonsweb.io`
- `ffjolstad.no`
- `fpangrand.fr`
- `fln404.ru`
- `f9v5.de`
- `fkanela.cz`
