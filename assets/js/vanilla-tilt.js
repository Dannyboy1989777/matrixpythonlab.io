/*

  tilt.js
  =======

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1

*/

import VanillaTilt from 'vanilla-tilt';

VanillaTilt.init(document.querySelectorAll('.hero-image-tilt'), {
    max: 3,
    speed: 2000,
    reverse: true,
    'mouse-event-element': '.home',
});

VanillaTilt.init(document.querySelectorAll('.card-tilt'), {
    max: 5,
    reverse: true,
    speed: 2000,
    scale: 1.025,
    glare: true,
    'max-glare': 0.05,
    perspective: 1000,
});
