/*

  ScrollReveal
  =============

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1

*/

import ScrollReveal from 'scrollreveal'


document.sr = ScrollReveal();

document.sr.reveal('.sr-bottom', {
    duration: 1500,
    origin: 'bottom',
    distance: '300px',
});

/*
sr.reveal('.sr-right', {
    duration: 2000,
    origin: 'right',
    distance: '300px',
});
sr.reveal('.sr-left', {
    duration: 2000,
    origin: 'left',
    distance: '300px',
});
*/
