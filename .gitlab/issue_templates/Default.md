Before raising an issue to the issue tracker, please read our Code of Conduct:

https://matrixpython.gitlab.io/docs/code_of_conduct/

You find more information in choosing the correct issue template on:

https://matrixpython.gitlab.io/wiki/Help:Workflow/#create-an-issue

Thank you for helping to improve our website!
