# The Python Community on Matrix Website

We are the _Python Community_ on Matrix, a free and open network for secure,
decentralized communication.

In this repository you find the sources to our website.

## Website Status

This website is still in development.

You can find the latest development build on
[https://matrixpython.gitlab.io](https://matrixpython.gitlab.io).

### Disclaimer

This project has not been released yet.
The information you find on it might be incomplete, or incorrect.
The current state does not represent the user experience in the
final stage. We are no professional web developers, and we do that in our spare
time, together with other projects and the moderation of the community.
So it might take a little longer until we release the page.
But we will get there.

## Contributing

If you found a defect, or you want to get involved with the development of our
website, check our [Contributing Wiki Page](
https://matrixpython.gitlab.io/wiki/Help:Introduction/). There you will find
all necessary information to get started, how our triage process works and
the documentation.

## Contact

We have prepared an
[FAQ section](https://matrixpython.gitlab.io/docs/help/faq/), which might
answer your questions. If not, simply
[contact us](https://matrixpython.gitlab.io/contact/).

## License

Designed by the Python Community on Matrix team with the help of our
[contributors](https://matrixpython.gitlab.io/contributors/) ❤️.<br />
Based on [Doks](https://getdoks.org/"), built with
[Hugo](https://gohugo.io/).<br />
Code licensed
[MIT](
https://gitlab.com/matrixpython/matrixpython.gitlab.io/blob/master/LICENSE.md),
docs
[CC BY 4.0](
https://gitlab.com/matrixpython/matrixpython.gitlab.io/blob/master/LICENSE_DOCS.md).

The license of this repositories logo: https://gitlab.com/matrixpython/designs/-/blob/main/gitlab_logos/website_logo
