const autoprefixer = require('autoprefixer');
const purgecss = require('@fullhuman/postcss-purgecss');
const whitelister = require('purgecss-whitelister');

module.exports = {
    syntax: 'postcss-scss',
    plugins: [
        autoprefixer(),
        purgecss({
            content: ['./layouts/**/*.html', './content/**/*.md'],
            safelist: [
                'lazyloaded',
                'table',
                'thead',
                'tbody',
                'tr',
                'th',
                'td',
                'h5',
                'alert-link',
                'container-xxl',
                'container-fluid',
                'offcanvas-backdrop',
                ...whitelister([
                    './assets/scss/components/_buttons.scss',
                    './assets/scss/components/_code.scss',
                    './assets/scss/components/_syntax.scss',
                    './assets/scss/components/_search.scss',
                    './assets/scss/components/_shortcode_alert.scss',
                    './assets/scss/common/_dark.scss',
                    './assets/scss/vendor/fork-awesome/**/*.scss',
                    './node_modules/bootstrap/scss/_dropdown.scss',
                    './node_modules/katex/dist/katex.css',
                    './assets/scss/components/_medium_zoom.scss',
                    './node_modules/highlight.js/scss/hybrid.scss',
                ]),
            ],
        }),
    ],
};
